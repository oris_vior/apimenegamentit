-- Создаем таблицу для проектов
CREATE TABLE projects (
                          project_id SERIAL PRIMARY KEY,
                          name TEXT NOT NULL,
                          description TEXT NOT NULL

);

-- Создаем таблицу для сотрудников
CREATE TABLE employees (
                           employee_id SERIAL PRIMARY KEY,
                           name TEXT NOT NULL,
                           email TEXT NOT NULL,
                           phone TEXT NOT NULL,
                           address TEXT NOT NULL,
                           specialization TEXT NOT NULL,
                           level TEXT NOT NULL

);

-- Создаем таблицу для привязки сотрудников к проектам
CREATE TABLE employee_projects (
                                   employee_id INTEGER,
                                   project_id INTEGER,
                                   PRIMARY KEY (employee_id, project_id),
                                   FOREIGN KEY (employee_id) REFERENCES employees(employee_id),
                                   FOREIGN KEY (project_id) REFERENCES projects(project_id)
);

