package com.example.ITManagementAPI.exception;

import org.postgresql.util.PSQLException;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionApiHandler {

    @ExceptionHandler(NullPointerException.class)
    public String handleException(NullPointerException e) {
        return e.getMessage();
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public String handleException(HttpMessageNotReadableException e) {
        return e.getMessage();
    }

    @ExceptionHandler(PSQLException.class)
    public String handleException(PSQLException e) {
        return e.getMessage();
    }

    @ExceptionHandler(HttpMessageNotWritableException.class)
    public String handleException(HttpMessageNotWritableException e) {
        return e.getMessage();
    }

    @ExceptionHandler(ChangeSetPersister.NotFoundException.class)
    public String handleException(ChangeSetPersister.NotFoundException e) {
        return e.getMessage();
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public String handleException(HttpMediaTypeNotSupportedException e) {
        return e.getMessage();
    }

    @ExceptionHandler(NumberFormatException.class)
    public String handleException(NumberFormatException e) {
        return e.getMessage();
    }

    @ExceptionHandler(RuntimeException.class)
    public String handleException(RuntimeException e) {
        return e.getMessage();
    }
}