package com.example.ITManagementAPI.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EmployeeProjectDto {
    private Long employee;
    private Long project;
}
