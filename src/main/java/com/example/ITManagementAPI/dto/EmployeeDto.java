package com.example.ITManagementAPI.dto;

import com.example.ITManagementAPI.model.enums.Level;
import com.example.ITManagementAPI.model.enums.Specialization;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EmployeeDto {
    private Long id;
    private String name;
    private String email;
    private String phone;
    private String address;
    private Specialization specialization;
    private Level level;
}
