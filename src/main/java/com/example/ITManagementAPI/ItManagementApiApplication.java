package com.example.ITManagementAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItManagementApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItManagementApiApplication.class, args);
	}

}
