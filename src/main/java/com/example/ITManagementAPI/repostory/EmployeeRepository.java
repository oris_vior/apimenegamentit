package com.example.ITManagementAPI.repostory;

import com.example.ITManagementAPI.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Override
    <S extends Employee> S save(S entity);

    List<Employee> findByNameOrEmailOrSpecializationOrLevel(String name, String email, String specialization, String level);

    @Override
    Optional<Employee> findById(Long aLong);

    @Override
    void deleteById(Long aLong);

    @Transactional
    @Modifying
    @Query("update Employee e set e.name = :name, e.email = :email, e.phone = :phone, e.address = :address ,e.specialization = :specialization,e.level = :level where e.id = :id")
    void updateEmployee(@Param("id") Long employeeId, @Param("name") String name, @Param("email") String email, @Param("phone") String phone, @Param("address") String address, @Param("specialization") String specialization, @Param("level") String level);

    @Override
    void deleteAll();
}


