package com.example.ITManagementAPI.repostory;

import com.example.ITManagementAPI.model.EmployeeProjects;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeProjectRepository extends JpaRepository<EmployeeProjects, Long> {

    @Transactional
    @Modifying
    @Query(value = "insert into employee_projects (employee_id, project_id) VALUES (:employeeId, :projectId)", nativeQuery = true)
    Integer saveEmployeeProject(@Param("employeeId") Integer employeeId, @Param("projectId") Integer projectId);

    @Query(value = "select employee_id  from employee_projects where project_id = :projectId", nativeQuery = true)
    List<Integer> findEmployee(@Param("projectId") Integer projectId);

    @Query(value = "select project_id  from employee_projects where employee_id = :employee_id", nativeQuery = true)
    List<Integer> findProject(@Param("employee_id") Integer employee_id);

    @Override
    Optional<EmployeeProjects> findById(Long aLong);

    @Transactional
    @Modifying
    @Query("delete from EmployeeProjects e where e.employee.employee_id = :employee_id and e.project.project_id = :project_id")
    void delete(@Param("employee_id") Integer employeeId, @Param("project_id") Integer projectId);

    @Transactional
    @Modifying
    @Query("update EmployeeProjects  e SET e.project.project_id=:project_id where e.employee.employee_id = :employee_id")
    void update(@Param("employee_id") Integer employeeId, @Param("project_id") Integer projectId);
}
