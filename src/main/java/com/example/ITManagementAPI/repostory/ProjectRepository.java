package com.example.ITManagementAPI.repostory;

import com.example.ITManagementAPI.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    @Override
    <S extends Project> S save(S entity);

    List<Project> findByNameOrDescription(String name, String description);

    @Override
    Optional<Project> findById(Long s);

    @Override
    void deleteById(Long s);

    @Transactional
    @Modifying
    @Query("update Project p set p.name = :name, p.description = :description  where p.id = :id")
    void updateProject(@Param("id") Long projectId, @Param("name") String name, @Param("description") String description);
}
