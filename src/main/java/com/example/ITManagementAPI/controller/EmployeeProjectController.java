package com.example.ITManagementAPI.controller;

import com.example.ITManagementAPI.dto.EmployeeDto;
import com.example.ITManagementAPI.dto.EmployeeProjectDto;
import com.example.ITManagementAPI.dto.ProjectDto;
import com.example.ITManagementAPI.service.EmployerProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee-project")
public class EmployeeProjectController {
    @Autowired
    private EmployerProjectService employerProjectService;

    @PostMapping("/add")
    public ResponseEntity createEmployeeProjects(@RequestBody EmployeeProjectDto employeeProjectDto) {
        employerProjectService.createEmployeeProjects(employeeProjectDto);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("/findProjectEmployees/{id}")
    public List<EmployeeDto> findEmployeesWhichWorksOnIdProject(@PathVariable Long id) {
        return employerProjectService.findEmployees(id);
    }

    @GetMapping("/findEmployeeProjects/{id}")
    public List<ProjectDto> findProjectFromIdEmployee(@PathVariable Long id) {
        return employerProjectService.findProject(id);
    }

    @DeleteMapping("/delete/employeeId/{employeeId}/projectId/{projectId}")
    public void deleteEmployeeProject(@PathVariable Long employeeId, @PathVariable Long projectId) {
        employerProjectService.delete(employeeId, projectId);
    }

    @PutMapping("/update_Employee/employeeId/{employeeId}/projectId/{projectId}")
    public void updateEmployeeProject(@PathVariable Long employeeId, @PathVariable Long projectId){
        employerProjectService.update(employeeId, projectId);
    }
}