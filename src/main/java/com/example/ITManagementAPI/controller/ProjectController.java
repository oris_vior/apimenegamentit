package com.example.ITManagementAPI.controller;

import com.example.ITManagementAPI.dto.ProjectDto;
import com.example.ITManagementAPI.model.Project;
import com.example.ITManagementAPI.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @PostMapping("/addNewProject")
    public ResponseEntity<ProjectDto> createNewProject(@RequestBody ProjectDto projectDto) {
        projectService.addNewProject(projectDto);
        return new ResponseEntity<>(projectDto, HttpStatus.CREATED);
    }

    @GetMapping("/findProject")
    public List<Project> findProject(@RequestParam(required = false) String name, @RequestParam(required = false) String description) {
        return projectService.findProject(name, description);
    }

    @DeleteMapping("/deleteProject/{id}")
    public void deleteProject(@PathVariable Long id) {
        projectService.delete(id);
    }

    @PutMapping("/updateProject/{id}")
    public ResponseEntity<ProjectDto> updateProject(@PathVariable Long id, @RequestBody ProjectDto projectDto) {
        return new ResponseEntity<>(projectService.updateProject(id, projectDto), HttpStatus.ACCEPTED);
    }
}
