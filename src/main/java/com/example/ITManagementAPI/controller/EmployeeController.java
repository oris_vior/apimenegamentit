package com.example.ITManagementAPI.controller;

import com.example.ITManagementAPI.dto.EmployeeDto;
import com.example.ITManagementAPI.model.Employee;
import com.example.ITManagementAPI.service.EmployerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
    @Autowired
    private EmployerService employerService;

    @PostMapping("/createNewEmployee")
    public ResponseEntity<EmployeeDto> createEmployee(@RequestBody EmployeeDto employeeDto) {
        employerService.createEmployee(employeeDto);
        return new ResponseEntity<>(employeeDto, HttpStatus.CREATED);
    }

    @GetMapping("/getByParams")
    public List<Employee> searchEmployees(@RequestParam(required = false) String name, @RequestParam(required = false) String email, @RequestParam(required = false) String specialization, @RequestParam(required = false) String level) {
        return employerService.findEmployeesByCriteria(name, email, specialization, level);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteEmployee(@PathVariable Long id) {
        employerService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/updateEmployee/{id}")
    public ResponseEntity<EmployeeDto> updateEmployee(@PathVariable Long id, @RequestBody EmployeeDto employeeDto) {
        return new ResponseEntity<>(employerService.updateEmployee(id, employeeDto), HttpStatus.ACCEPTED);
    }
}