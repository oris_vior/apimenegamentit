package com.example.ITManagementAPI.model;

import jakarta.persistence.*;
import lombok.Data;


@Entity
@Table(name = "employee_projects")
@Data
public class EmployeeProjects {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;
}
