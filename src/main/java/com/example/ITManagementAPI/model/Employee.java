package com.example.ITManagementAPI.model;

import jakarta.persistence.*;
import lombok.Data;


@Entity
@Table(name = "employees")
@Data
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long employee_id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "address")
    private String address;

    @Column(name = "specialization")
    private String specialization;

    @Column(name = "level")
    private String level;
}
