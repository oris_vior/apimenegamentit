package com.example.ITManagementAPI.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "projects")
@Data
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long project_id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
}