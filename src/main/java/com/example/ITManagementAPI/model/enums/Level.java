package com.example.ITManagementAPI.model.enums;

public enum Level {
    JUNIOR,
    MIDDLE,
    SENIOR
}
