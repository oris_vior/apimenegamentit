package com.example.ITManagementAPI.model.enums;

public enum Specialization {
    Developer,
    QA,
    DevOps,
    ProjectManager
}
