package com.example.ITManagementAPI.service;

import com.example.ITManagementAPI.dto.ProjectDto;
import com.example.ITManagementAPI.model.Project;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProjectService {
    Project addNewProject(ProjectDto projectDto);
    List<Project> findProject(String name, String description);
    void delete(Long id);
    ProjectDto updateProject(Long id, ProjectDto projectDto);
}
