package com.example.ITManagementAPI.service;

import com.example.ITManagementAPI.dto.ProjectDto;
import com.example.ITManagementAPI.model.Project;
import com.example.ITManagementAPI.repostory.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {
    @Autowired
    ProjectRepository projectRepository;

    @Override
    public Project addNewProject(ProjectDto projectDto) {
        return projectRepository.save(convertFromDtoToProject(projectDto));
    }

    @Override
    public List<Project> findProject(String name, String description) {
        return projectRepository.findByNameOrDescription(name, description);
    }

    @Override
    public void delete(Long id) {
        projectRepository.deleteById(id);
    }

    @Override
    public ProjectDto updateProject(Long id, ProjectDto projectDto) {
        Project project;
        project = projectRepository.findById(id).get();
        projectRepository.updateProject(project.getProject_id(), projectDto.getName(), projectDto.getDescription());
        project = projectRepository.findById(id).get();
        return convertFromProjectToDto(project);
    }

    private Project convertFromDtoToProject(ProjectDto projectDto) {
        Project project = new Project();
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        return project;
    }

    private ProjectDto convertFromProjectToDto(Project project) {

        return ProjectDto.builder().name(project.getName()).description(project.getDescription()).build();
    }
}
