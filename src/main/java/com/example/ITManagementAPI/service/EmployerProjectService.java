package com.example.ITManagementAPI.service;

import com.example.ITManagementAPI.dto.EmployeeDto;
import com.example.ITManagementAPI.dto.EmployeeProjectDto;
import com.example.ITManagementAPI.dto.ProjectDto;

import java.util.List;

public interface EmployerProjectService {
    Integer createEmployeeProjects(EmployeeProjectDto employeeProjectDto);

    List<EmployeeDto> findEmployees(Long id);

    List<ProjectDto> findProject(Long id);

    void delete(Long employeeId, Long projectId);

    void update(Long employeeId, Long projectId);
}
