package com.example.ITManagementAPI.service;

import com.example.ITManagementAPI.dto.EmployeeDto;
import com.example.ITManagementAPI.dto.EmployeeProjectDto;
import com.example.ITManagementAPI.dto.ProjectDto;
import com.example.ITManagementAPI.model.Employee;
import com.example.ITManagementAPI.model.Project;
import com.example.ITManagementAPI.model.enums.Level;
import com.example.ITManagementAPI.model.enums.Specialization;
import com.example.ITManagementAPI.repostory.EmployeeProjectRepository;
import com.example.ITManagementAPI.repostory.EmployeeRepository;
import com.example.ITManagementAPI.repostory.ProjectRepository;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployerProjectServiceImpl implements EmployerProjectService {
    @Autowired
    EmployeeProjectRepository employeeProjectRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    ProjectRepository projectRepository;

    @SneakyThrows
    @Override
    public Integer createEmployeeProjects(EmployeeProjectDto employeeProjectsDto) {
        Employee employee = employeeRepository.findById(employeeProjectsDto.getEmployee())
                .orElseThrow(ChangeSetPersister.NotFoundException::new);
        Project project = projectRepository.findById(employeeProjectsDto.getProject())
                .orElseThrow(ChangeSetPersister.NotFoundException::new);

        return employeeProjectRepository.saveEmployeeProject(employee.getEmployee_id()
                .intValue(), project.getProject_id().intValue());
    }

    @Override
    public List<EmployeeDto> findEmployees(Long id) {
        List<Integer> idEmployees = employeeProjectRepository.findEmployee(id.intValue());
        List<EmployeeDto> employeeDtoList = new ArrayList<>();
        for (Integer i : idEmployees) {
            employeeDtoList.add(convertFromEmployeeToDto(employeeRepository.findById(i.longValue()).get()));
        }
        return employeeDtoList;
    }

    @Override
    public List<ProjectDto> findProject(Long id) {
        List<Integer> idProjects = employeeProjectRepository.findProject(id.intValue());
        List<ProjectDto> projectDtoList = new ArrayList<>();
        for (Integer i : idProjects) {
            projectDtoList.add(convertFromProjectToDto(projectRepository.findById(i.longValue()).get()));
        }
        return projectDtoList;
    }

    @Override
    public void delete(Long employeeId, Long projectId) {
        employeeProjectRepository.delete(employeeId.intValue(), projectId.intValue());
    }

    @Override
    public void update(Long employeeId, Long projectId) {
        employeeProjectRepository.update(employeeId.intValue(), projectId.intValue());
    }

    private EmployeeDto convertFromEmployeeToDto(Employee employee) {
        return EmployeeDto.builder()
                .id(employee.getEmployee_id())
                .name(employee.getName())
                .phone(employee.getPhone())
                .address(employee.getAddress())
                .email(employee.getEmail())
                .specialization(Specialization.valueOf(employee.getSpecialization()))
                .level(Level.valueOf(employee.getLevel()))
                .build();
    }

    private ProjectDto convertFromProjectToDto(Project project) {
        return ProjectDto.builder()
                .id(project.getProject_id())
                .name(project.getName())
                .description(project.getDescription())
                .build();
    }
}
