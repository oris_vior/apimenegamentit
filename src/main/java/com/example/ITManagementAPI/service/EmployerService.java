package com.example.ITManagementAPI.service;

import com.example.ITManagementAPI.dto.EmployeeDto;
import com.example.ITManagementAPI.model.Employee;

import java.util.List;

public interface EmployerService {
    EmployeeDto createEmployee(EmployeeDto employeeDto);
    List<Employee> findEmployeesByCriteria(String name, String email, String specialization, String level);
    void delete(Long id);
    EmployeeDto updateEmployee(Long id, EmployeeDto employeeDto);
    void deleteAll();
}
