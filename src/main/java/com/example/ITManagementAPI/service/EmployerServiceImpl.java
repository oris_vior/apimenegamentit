package com.example.ITManagementAPI.service;

import com.example.ITManagementAPI.dto.EmployeeDto;
import com.example.ITManagementAPI.model.Employee;
import com.example.ITManagementAPI.model.enums.Level;
import com.example.ITManagementAPI.model.enums.Specialization;
import com.example.ITManagementAPI.repostory.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployerServiceImpl implements EmployerService {
    @Autowired
    private EmployeeRepository employeeRepository;


    @Override
    public EmployeeDto createEmployee(EmployeeDto employeeDto) {
        return convertFromEmployeeToDto(employeeRepository.save(convertFromDtoToEmployee(employeeDto)));
    }

    @Override
    public List<Employee> findEmployeesByCriteria(String name, String email, String specialization, String level) {
        return employeeRepository.findByNameOrEmailOrSpecializationOrLevel(name, email, specialization, level);
    }

    @Override
    public void delete(Long id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public EmployeeDto updateEmployee(Long id, EmployeeDto employeeDto) {
        Employee employee;
        employee = employeeRepository.findById(id).get();
        employeeRepository.updateEmployee(employee.getEmployee_id(), employeeDto.getName(), employeeDto.getEmail(), employeeDto.getPhone(), employeeDto.getAddress(), employeeDto.getSpecialization()
                .toString(), employeeDto.getLevel().toString());
        employee = employeeRepository.findById(id).get();

        return convertFromEmployeeToDto(employee);
    }

    private Employee convertFromDtoToEmployee(EmployeeDto employeeDto) {
        Employee employee = new Employee();
        employee.setName(employeeDto.getName());
        employee.setEmail(employeeDto.getEmail());
        employee.setPhone(employeeDto.getPhone());
        employee.setAddress(employeeDto.getAddress());
        employee.setSpecialization(employeeDto.getSpecialization().toString());
        employee.setLevel(employeeDto.getLevel().toString());
        return employee;
    }

    private EmployeeDto convertFromEmployeeToDto(Employee employee) {
        return EmployeeDto.builder()
                .id(employee.getEmployee_id())
                .name(employee.getName())
                .phone(employee.getPhone())
                .address(employee.getAddress())
                .email(employee.getEmail())
                .specialization(Specialization.valueOf(employee.getSpecialization()))
                .level(Level.valueOf(employee.getLevel()))
                .build();
    }

    public void deleteAll() {
        employeeRepository.deleteAll();
    }
}
