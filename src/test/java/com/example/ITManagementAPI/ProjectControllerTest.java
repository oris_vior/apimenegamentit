package com.example.ITManagementAPI;

import com.example.ITManagementAPI.dto.ProjectDto;
import com.example.ITManagementAPI.model.Project;
import com.example.ITManagementAPI.service.ProjectService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ProjectControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProjectService projectService;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void cleanUp() {
        jdbcTemplate.execute("DELETE FROM projects");
    }

    @Test
    public void testCreateNewProject() throws Exception {
        ProjectDto projectDto = ProjectDto.builder().name("Test").description("DES_Test").build();
        mockMvc.perform(post("/project/addNewProject").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(projectDto))).andExpect(status().isCreated());

        List<Project> projects = projectService.findProject("Test", "DES_Test");
        Assertions.assertEquals(projects.size(), 1);
        Project project = projects.get(0);
        Assertions.assertEquals(project.getName(), "Test");
    }

    @Test
    public void testFindProject() throws Exception {
        ProjectDto projectDto1 = ProjectDto.builder().name("Test1").description("DES_Test1").build();
        ProjectDto projectDto2 = ProjectDto.builder().name("Test2").description("DES_Test2").build();
        projectService.addNewProject(projectDto1);
        projectService.addNewProject(projectDto2);
        mockMvc.perform(get("/project/findProject").param("name", "Test1").param("description", "DES_Test1"))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteProject() throws Exception {
        ProjectDto projectDto1 = ProjectDto.builder().name("Test1").description("DES_Test1").build();
        Project project = projectService.addNewProject(projectDto1);
        mockMvc.perform(delete("/project/deleteProject/{id}", project.getProject_id())).andExpect(status().isOk());
        List<Project> projects = projectService.findProject("Test1", "DES_Test1");
        Assertions.assertEquals(projects.size(), 0);
    }
}
