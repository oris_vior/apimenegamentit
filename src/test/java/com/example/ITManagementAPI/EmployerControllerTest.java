package com.example.ITManagementAPI;

import com.example.ITManagementAPI.dto.EmployeeDto;
import com.example.ITManagementAPI.model.Employee;
import com.example.ITManagementAPI.model.enums.Level;
import com.example.ITManagementAPI.model.enums.Specialization;
import com.example.ITManagementAPI.service.EmployerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class EmployerControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private EmployerService employerService;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void cleanUp() {
        jdbcTemplate.execute("DELETE FROM employees");
    }

    @Test
    public void testCreateEmployee() throws Exception {
        EmployeeDto employeeDto = EmployeeDto.builder()
                .name("Test")
                .phone("test")
                .address("test address")
                .email("test@gmail.com")
                .specialization(Specialization.Developer)
                .level(Level.SENIOR)
                .build();
        mockMvc.perform(post("/employees/createNewEmployee").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(employeeDto))).andExpect(status().isCreated());
        List<Employee> employees = employerService.findEmployeesByCriteria("Test", "test@gmail.com", "Developer", "SENIOR");
        Assertions.assertEquals(employees.size(), 1);
        Assertions.assertEquals(employees.get(0).getName(), "Test");
        Assertions.assertEquals(employees.get(0).getSpecialization(), "Developer");
    }

    @Test
    public void testSearchEmployees() throws Exception {
        EmployeeDto employeeDto1 = EmployeeDto.builder()
                .name("Test1")
                .phone("test1")
                .address("test1 address")
                .email("tes1t@gmail.com")
                .specialization(Specialization.Developer)
                .level(Level.SENIOR)
                .build();

        EmployeeDto employeeDto2 = EmployeeDto.builder()
                .name("Test1")
                .phone("test1")
                .address("test1 address")
                .email("test1@gmail.com")
                .specialization(Specialization.Developer)
                .level(Level.MIDDLE)
                .build();
        employerService.createEmployee(employeeDto1);
        employerService.createEmployee(employeeDto2);
        mockMvc.perform(get("/employees/getByParams").param("name", "Test1").param("specialization", "Developer"))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteEmployee() throws Exception {
        EmployeeDto employeeDto = EmployeeDto.builder()
                .name("Test1")
                .phone("test1")
                .address("test1 address")
                .email("tes1t@gmail.com")
                .specialization(Specialization.Developer)
                .level(Level.SENIOR)
                .build();
        EmployeeDto employee = employerService.createEmployee(employeeDto);
        mockMvc.perform(delete("/employees/delete/{id}", employee.getId())).andExpect(status().isOk());
        List<Employee> employees = employerService.findEmployeesByCriteria("Test1", "tes1t@gmail.com", "Developer", "SENIOR");
        Assertions.assertEquals(employees.size(), 0);
    }
}
