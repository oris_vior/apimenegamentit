# Api_Menegament_IT

RESTfull web сервіс для внутрішнього керування компанією. Команди складаються з менеджерів і програмістів різних рівнів(Jun/Mid/Senior) та видів(developer, QA, DevOps). Всі вони можуть працювати на кількох проектах одночасно.

## Getting Started

1) Вказати налаштуання в application.properties для PostgreSQL
2) Запутити скрипт data.sql для створення таблиць в БД
3) Запустити програму

## Controllers

### Employees
+ localhost:8080/employees/createNewEmployee
+ localhost:8080/employees/getByParams
+ localhost:8080/employees/delete/{id}
+ localhost:8080/employees/updateEmployee/{id}
### Project
+ localhost:8080/project/addNewProject
+ localhost:8080/project/findProject?name={name}
+ localhost:8080/project/deleteProject/{id}
+ localhost:8080/project/updateProject/{id}
### Employee-Project
+ localhost:8080/employee-project/add
+ localhost:8080/employee-project/findProjectEmployees/{id}
+ localhost:8080/employee-project/delete/employeeId/{employeeId}/projectId/{projectId}
+ localhost:8080/employee-project/update_Employee/employeeId/{employeeId}/projectId/{projectId}
## Running the tests
Тести за шляхом *"src/test/java/com/example/ITManagementAPI"*

## Authors

Author 1 - Bunevich Ihor
